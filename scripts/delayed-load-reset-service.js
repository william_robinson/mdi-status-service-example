function DelayedLoadResetServiceFactory(){
  var listeners = [];
  var svc = function(){
    listeners.forEach(function(listener){
      listener();
    });
  };
  
  svc.AddListener = function(listener){
    listeners.push(listener);
  };
  
  return svc;
}

app.factory("DelayedLoadReset", DelayedLoadResetServiceFactory);