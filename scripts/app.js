var app = angular.module('plunker', []);

function MainCtrl(mdiStatus, $log, DelayedLoadReset, $timeout, $interval) {
  var ctrl = this;
  ctrl.ClassificationOptions = ["Space Station", "Moon", "Planet"];
  
  ctrl.Initialize = function(){
    ctrl.Initialized = true;
    ctrl.ReadyToFire = false;
    ctrl.IgnitionReady = false;
    ctrl.TargetConfirmed = false;
    ctrl.Coordinates = "";
    ctrl.Classification = 0;
    ctrl.IgnitionTaskReady = [];
    ctrl.ShowFire = false;
    ctrl.ShowMonster = false;
    ctrl.FireImage = "";
  };
 
  ctrl.Initialize();
  
  ctrl.Reset = function(){
    DelayedLoadReset();
    ctrl.Initialize();
    mdiStatus.Reset();
  };
  
  ctrl.CoordinatesChanged = function(){
    mdiStatus.Dirty("coordinates");
  };
  
  ctrl.ClassificationChanged = function(){
    mdiStatus.Dirty("classification");
  };
  
  ctrl.ConfirmTarget = function(){
    mdiStatus.Clean("coordinates");
    mdiStatus.Clean("classification");
  };
  
  ctrl.Fire = function(){
    if(!ctrl.ReadyToFire){
      throw "Not ready to fire";
    }    
    
    ctrl.ShowFire = true;
    ctrl.FireImage = {
      "background-image": "url(http://fansided.com/files/2014/12/Ku7mUmo.gif)"
    };
    $timeout(function(){
      ctrl.Reset();
    }, 4250);
    $interval(function(){
      ctrl.ShowMonster = !ctrl.ShowMonster;
    }, 500, 5);
  };
  
  mdiStatus.AddReadyListener(
    function(status){
      $log.debug("Ready status:" + status.Loading + " loading, " + status.Ready + " ready." );
      ctrl.Initialized = !(status.Loading || status.Ready);
      ctrl.IgnitionReady = status.Ready === 3;
      ctrl.Waiting = status.Loading;
      
      while(ctrl.IgnitionTaskReady.length < status.Ready){
        ctrl.IgnitionTaskReady.push(status.Ready);
      }
      while(ctrl.IgnitionTaskReady.length > status.Ready){
        ctrl.IgnitionTaskReady.pop();
      }
    });
    
  mdiStatus.AddDirtyListener(
    function(status){
      $log.debug("Dirty status:" + status.Dirty + " dirty, " + status.Clean + " clean.");
      ctrl.TargetConfirmed = status.Clean == 2;
      ctrl.TargetConfigured = status.Dirty + status.Clean == 2 && status.Dirty;
    });
  
  mdiStatus.AddCompleteListener(
    function(status){
      $log.debug(
        "Complete status: dirty " 
        + status.Dirty + "/" + (status.Dirty + status.Clean) 
        + ", loading " 
        + status.Loading + "/" + (status.Loading + status.Ready));
      ctrl.ReadyToFire = status.Clean == 2 && status.Ready == 3;  
      
    });
}

MainCtrl.$inject = ["mdiStatus", "$log", "DelayedLoadReset", "$timeout", "$interval"];

app.controller('MainCtrl', MainCtrl);
