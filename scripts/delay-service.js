function DelayServiceFactory($q, $timeout, RandomInteger){
  var svc = function(maxSec){
    delayMs = RandomInteger(maxSec) * 1000;
    var task = $q.defer();
    $timeout(function(){
      task.resolve();
    }, delayMs);
    
    return task.promise;
  }
  
  return svc;
}

DelayServiceFactory.$inject = ["$q", "$timeout", "RandomInteger"];
app.factory("Delay", DelayServiceFactory);