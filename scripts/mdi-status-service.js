(function () {
    function mdiStatusServiceFactory() {
        var svc = {};
        var reporterNameArray = [];
        var reporterArray = [];
        var readyListeners = [];
        var dirtyListeners = [];
        var completeListeners = [];

        var SetStatus = function (name, statuses) {
            if (!reporterArray[name]) {
                reporterArray[name] = {};
                reporterNameArray.push(name);
            }

            if (statuses.IsReady !== undefined) {
                reporterArray[name].IsReady = statuses.IsReady;
            }

            if (statuses.IsDirty !== undefined) {
                reporterArray[name].IsDirty = statuses.IsDirty;
            }
        };

        var GetReadyCounts = function () {
            var loadingCount = 0;
            var readyCount = 0;
            for (var reporter in reporterArray) {
                if (reporterArray[reporter].IsReady) {
                    readyCount++;
                }
                if (reporterArray[reporter].IsReady === false) {
                    loadingCount++;
                }
            }

            return {
                Ready: readyCount,
                Loading: loadingCount,
            };
        };

        var ReportReady = function () {
            var readyCounts = GetReadyCounts();
            readyListeners.forEach(
              function (listener) {
                  listener(readyCounts);
              }
            );
        };

        var GetDirtyCounts = function () {
            var dirtyCount = 0;
            var cleanCount = 0;
            for (var reporter in reporterArray) {
                if (reporterArray[reporter].IsDirty) {
                    dirtyCount++;
                }
                if (reporterArray[reporter].IsDirty === false) {
                    cleanCount++;
                }
            }

            return {
                Dirty: dirtyCount,
                Clean: cleanCount,
            };
        };

        var ReportDirty = function () {
            var dirtyCounts = GetDirtyCounts();
            dirtyListeners.forEach(
              function (listener) {
                  listener(dirtyCounts);
              });
        };

        var GetCompleteStatus = function () {
            var ready = GetReadyCounts();
            var dirty = GetDirtyCounts();
            return {
                Ready: ready.Ready,
                Loading: ready.Loading,
                Dirty: dirty.Dirty,
                Clean: dirty.Clean,
            }
        }

        var ReportComplete = function () {
            var status = GetCompleteStatus();
            completeListeners.forEach(
              function (listener) {
                  listener(status);
              });
        };

        svc.Dirty = function (name) {
            if(!name){ return; }
            SetStatus(name, { IsDirty: true });
            ReportDirty();
            ReportComplete();
        };

        svc.Clean = function (name) {
            if(!name){ return; }
            SetStatus(name, { IsDirty: false });
            ReportDirty();
            ReportComplete();
        };

        svc.Loading = function (name) {
            if(!name){ return; }
            SetStatus(name, { IsReady: false });
            ReportReady();
            ReportComplete();
        };

        svc.Ready = function (name) {
            if(!name){ return; }
            SetStatus(name, { IsReady: true });
            ReportReady();
            ReportComplete();
        };

        svc.AddReadyListener = function (listener) {
            if (!listener) { return; }

            readyListeners.push(listener);
            ReportReady();
            ReportComplete();
        };
        svc.AddLoadingListener = svc.AddReadyListener;

        svc.AddDirtyListener = function (listener) {
            if (!listener) { return; }

            dirtyListeners.push(listener);
            ReportDirty();
            ReportComplete();
        };
        svc.AddCleanListener = svc.AddDirtyListener;

        svc.AddCompleteListener = function (listener) {
            if (!listener) { return; }

            completeListeners.push(listener);
            ReportComplete();
        };

        var GetReporterStatusList = function (status, value) {
            var list = [];
            reporterNameArray.forEach(function (reporterName) {
                if (reporterArray[reporterName][status] === value) {
                    list.push(reporterName);
                }
            });
            return list;
        };

        svc.GetDirtyList = function () {
            return GetReporterStatusList('IsDirty', true);
        };
        svc.GetCleanList = function () {
            return GetReporterStatusList('IsDirty', false);
        };
        svc.GetLoadingList = function () {
            return GetReporterStatusList('IsReady', false);
        };
        svc.GetReadyList = function () {
            return GetReporterStatusList('IsReady', true);
        };

        svc.Reset = function () {
            reporterArray = [];
            reporterNameArray = [];
            ReportDirty();
            ReportReady();
            ReportComplete();
        };

        return svc;
    }

    app.factory("mdiStatus", mdiStatusServiceFactory);
})();
