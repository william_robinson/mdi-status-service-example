function RandomIntegerService(){
    var svc = function(max){
        if(isNaN(max)){
            throw "Number required.";
        }
        return Math.ceil(Math.random() * max);
    }
    
    return svc;
}

app.factory("RandomInteger", RandomIntegerService);