var DelayedLoadDirective = function(Delay, mdiStatus, DelayedLoadReset, $log) {
  var DelayedLoadLink = function(scope) {
    var Reset = function(cancel){
      scope.Status = "Ready";
      scope.Loaded = false;
    };

    scope.Load = function() {
      scope.Loaded = false;
      scope.Status = "Loading";
      mdiStatus.Loading(scope.Action);
      Delay(10).then( function(){
        scope.Loaded = true;
        scope.Status = "Ready";
        mdiStatus.Ready(scope.Action);
      });
    }
    
    Reset();
    DelayedLoadReset.AddListener(function(){ Reset(true); });
  };


  return {
    restrict: "E",
    link: DelayedLoadLink,
    templateUrl: "templates/delayed-load.html",
    scope: {
      Action: "@action"
    },
  };
};

DelayedLoadDirective.$inject = ["Delay", "mdiStatus", "DelayedLoadReset", "$log"];
app.directive("delayedLoad", DelayedLoadDirective);
