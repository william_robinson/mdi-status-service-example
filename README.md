Welcome to your introduction to the MDI Status Service. 

There are three sets of methods you'll need to use. The first is the reporting methods,
each of which you can call with a unique name for a component.

* Dirty(name)
* Clean(name)
* Loading(name)
* Ready(name)

Dirty and Clean are opposites, and so are Loading and Ready. So, if your
component's name is "hpi", you'd call Dirty("hpi") when its data changed, and 
Clean("hpi") when it was saved or reset. It's similar for Loading("hpi") and 
Ready("hpi").

The second set of methods are the listener-adding ones. You can call the first
two of these methods using aliases.

* AddDirtyListener(function) / AddCleanListener(function)
* AddLoadingListener(function) / AddReadyListener(function)
* AddCompleteListener(function)

Pass a callback function to one of these methods and that function will be
called whenever anything else reports a status change of that type. 

For example, if **onLoading** is a function, you'd call AddLoadingListener(onLoading).
Then, any time any code calls Loading() or Ready(), **onLoading** will be run.

The callbacks sent to AddCompleteListener() will be called when *any* of the
four reporting methods is called.

Finally, the following methods will return an array of strings identifying the components
that are in a certain status.

* GetCleanList()
* GetDirtyList()
* GetReadyList()
* GetLoadingList()

The status service also has a **Reset()** method, which removes all reported statuses and
immediately executes all callback functions.

The callback functions will be passed an object with the following properties:

* Dirty / Clean callback: { Dirty: 0, Clean: 0 }
* Loading / Ready callback: { Loading: 0, Ready: 0}
* Complete callback: { Dirty: 0, Clean: 0, Loading: 0, Ready: 0}

Each property's value will be the number of component's who have reported that 
status. 

**IMPORTANT:** A component will be included in the counts ONLY if they have
called a reporting method. So, callbacks will need to know how many components
to expect. For example, code that should be run when all of 6 components are "ready"
has to know how many components CAN be ready, and wait for
"{ Loading: 0, Ready: 6 }" to be returned from the status service.