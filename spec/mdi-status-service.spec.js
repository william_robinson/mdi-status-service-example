describe("Status Service", function(){
  beforeEach(module("plunker"));
  
  var provider;
  
  beforeEach(inject(function(mdiStatus){
    provider = mdiStatus;
  }));  
  
  describe("when a dirty listener is added", function(){
    var listener;
    
    beforeEach(function(){
      listener = jasmine.createSpy("listener");
      provider.AddDirtyListener(listener);
    });
  
    it("it is called once with no component statuses", function(){
      expect(listener).toHaveBeenCalledTimes(1);
      expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0});
    });

    describe("then, a dirty report is sent", function(){
      beforeEach(function(){
        provider.Dirty("component");
      });
      
      it("the listener is called again with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Dirty: 1, Clean: 0});
      });
    });

    describe("then, a clean report is sent", function(){
      beforeEach(function(){
        provider.Clean("component");
      });
      
      it("the listener is called again with one clean component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 1});
      });
    });
  });
  
  describe("when a clean listener is added", function(){
    var listener;
    
    beforeEach(function(){
      listener = jasmine.createSpy("listener");
      provider.AddCleanListener(listener);
    });
  
    it("it is called once with no component statuses", function(){
      expect(listener).toHaveBeenCalledTimes(1);
      expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0});
    });
    
    describe("then, a clean report is sent", function(){
      beforeEach(function(){
        provider.Clean("component");
      });
      
      it("the listener is called again with one clean component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 1});
      });
    });

    describe("then, a dirty report is sent", function(){
      beforeEach(function(){
        provider.Dirty("component");
      });
      
      it("the listener is called again with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Dirty: 1, Clean: 0});
      });
    });
  });  
  
  describe("when a ready listener is added", function(){
    var listener;
    
    beforeEach(function(){
      listener = jasmine.createSpy("listener");
      provider.AddReadyListener(listener);
    });
  
    it("it is called once with no component statuses", function(){
      expect(listener).toHaveBeenCalledTimes(1);
      expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 0 });
    });

    describe("then, a ready report is sent", function(){
      beforeEach(function(){
        provider.Ready("component");
      });
      
      it("the listener is called again with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 1 });
      });
    });

    describe("then, a loading report is sent", function(){
      beforeEach(function(){
        provider.Loading("component");
      });
      
      it("the listener is called again with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 1, Ready: 0 });
      });
    });
  });
  
  describe("when a loading listener is added", function(){
    var listener;
    
    beforeEach(function(){
      listener = jasmine.createSpy("listener");
      provider.AddLoadingListener(listener);
    });
  
    it("it is called once with no component statuses", function(){
      expect(listener).toHaveBeenCalledTimes(1);
      expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 0 });
    });
    
    describe("then, a loading report is sent", function(){
      beforeEach(function(){
        provider.Loading("component");
      });
      
      it("the listener is called again with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 1, Ready: 0 });
      });
    });

    describe("then, a ready report is sent", function(){
      beforeEach(function(){
        provider.Ready("component");
      });
      
      it("the listener is called again with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 1 });
      });
    });
  });  
  
  describe("when a complete listener is added", function(){
    var listener;
    
    beforeEach(function(){
      listener = jasmine.createSpy("listener");
      provider.AddCompleteListener(listener);
    });
  
    it("it is called once with no component statuses", function(){
      expect(listener).toHaveBeenCalledTimes(1);
      expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 0, Clean: 0 });
    });
    
    describe("then, a loading report is sent", function(){
      beforeEach(function(){
        provider.Loading("component");
      });
      
      it("the listener is called again with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 1, Ready: 0, Dirty: 0, Clean: 0 });
      });
    });

    describe("then, a ready report is sent", function(){
      beforeEach(function(){
        provider.Ready("component");
      });
      
      it("the listener is called again with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 1, Dirty: 0, Clean: 0 });
      });
    });
    
    describe("then, a dirty report is sent", function(){
      beforeEach(function(){
        provider.Dirty("component");
      });
      
      it("the listener is called again with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 1, Clean: 0 });
      });
    });

    describe("then, a clean report is sent", function(){
      beforeEach(function(){
        provider.Clean("component");
      });
      
      it("the listener is called again with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(2);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 0, Clean: 1 });
      });
    });
  });  
  
  describe("when a null listener is added", function(){
    it("do not throw an error", function(){
      expect(function(){provider.AddDirtyListener(null);}).not.toThrow();
      expect(function(){provider.AddCleanListener(null);}).not.toThrow();
      expect(function(){provider.AddLoadingListener(null);}).not.toThrow();
      expect(function(){provider.AddReadyListener(null);}).not.toThrow();
      expect(function(){provider.AddCompleteListener(null);}).not.toThrow();
    });
  });
  
  describe("when a null component name is used to report a status", function(){
    beforeEach(function(){
      listeners = {
        dirtyListener: function(){},
        cleanListener: function(){},
        loadingListener: function(){},
        readyListener: function(){},
        completeListener: function(){},
      }
      spyOn(listeners, "dirtyListener");
      spyOn(listeners, "cleanListener");
      spyOn(listeners, "loadingListener");
      spyOn(listeners, "readyListener");
      spyOn(listeners, "completeListener");
      
      provider.AddDirtyListener(listeners.dirtyListener);
      provider.AddCleanListener(listeners.cleanListener);
      provider.AddLoadingListener(listeners.loadingListener);
      provider.AddReadyListener(listeners.readyListener);
      provider.AddCompleteListener(listeners.completeListener);
      
      listeners.dirtyListener.calls.reset();
      listeners.cleanListener.calls.reset();
      listeners.loadingListener.calls.reset();
      listeners.readyListener.calls.reset();
      listeners.completeListener.calls.reset();
    });
    it("no component is added to the reporter list", function(){
      provider.Clean(null);
      provider.Dirty(null);
      provider.Ready(null);
      provider.Loading(null);      

      expect(provider.GetCleanList().length).toEqual(0);
      expect(provider.GetDirtyList().length).toEqual(0);
      expect(provider.GetReadyList().length).toEqual(0);
      expect(provider.GetLoadingList().length).toEqual(0);
    });
    it("no listeners are called", function(){

      provider.Clean(null);
      provider.Dirty(null);
      provider.Ready(null);
      provider.Loading(null);      
      expect(listeners.dirtyListener).not.toHaveBeenCalled();
      expect(listeners.cleanListener).not.toHaveBeenCalled();
      expect(listeners.readyListener).not.toHaveBeenCalled();
      expect(listeners.loadingListener).not.toHaveBeenCalled();
      expect(listeners.completeListener).not.toHaveBeenCalled();
    });
    it("no errors are thrown", function(){
      var clean = function(){provider.Clean(null);};
      var dirty = function(){provider.Dirty(null);};
      var ready = function(){provider.Ready(null);};
      var loading = function(){provider.Loading(null);};      

      expect(clean).not.toThrow();
      expect(dirty).not.toThrow();
      expect(ready).not.toThrow();
      expect(loading).not.toThrow();
    });
  });
  
  describe("when a component reports 'dirty'", function(){
    beforeEach(function(){
      provider.Dirty("component");
    });
    
    it("reporter list contains component name", function(){
      expect(provider.GetDirtyList()).toContain("component");
    });
    
    describe("then, a dirty listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddDirtyListener(listener);
      });

      it("the listener is called once with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 1, Clean: 0});
      });
    });
    
    describe("then, a clean listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddCleanListener(listener);
      });

      it("the listener is called once with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 1, Clean: 0});
      });
    });
    
    describe("then, a complete listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddCompleteListener(listener);
      });
      
      it("the listener is called once with one dirty component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 1, Clean: 0, Loading: 0, Ready: 0 });
      });
    });
  });
  
  describe("when a component reports 'clean'", function(){
    beforeEach(function(){
      provider.Clean("component");
    });
    
    it("reporter list contains component name", function(){
      expect(provider.GetCleanList()).toContain("component");
    });
    
    describe("then, a clean listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddDirtyListener(listener);
      });

      it("the listener is called once with one clean component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 1});
      });
    });
    
    describe("then, a dirty listener  is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddDirtyListener(listener);
      });

      it("the listener is called once with one clean component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 1});
      });
    });
    
    describe("then, complete listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddCompleteListener(listener);
      });
      
      it("the listener is called once with one clean component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 1, Loading: 0, Ready: 0 });
      });
    });
  });

  describe("when a component reports 'loading'", function(){
    beforeEach(function(){
      provider.Loading("component");
    });
    
    it("reporter list contains component name", function(){
      expect(provider.GetLoadingList()).toContain("component");
    });
    
    describe("then, loading listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddLoadingListener(listener);
      });

      it("the listener is called once with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Loading: 1, Ready: 0});
      });
    });
    
    describe("then, a ready listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddReadyListener(listener);
      });

      it("the listener is called once with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Loading: 1, Ready: 0});
      });
    });
    
    describe("then, a complete listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddCompleteListener(listener);
      });
      
      it("the listener is called once with one loading component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0, Loading: 1, Ready: 0 });
      });
    });
  });

  describe("when a component reports 'ready'", function(){
    beforeEach(function(){
      provider.Ready("component");
    });
    
    it("reporter list contains component name", function(){
      expect(provider.GetReadyList()).toContain("component");
    });
    
    describe("then, ready listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddReadyListener(listener);
      });

      it("the listener is called once with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 1});
      });
    });
    
    describe("then, a loading listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddLoadingListener(listener);
      });

      it("the listener is called once with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Loading: 0, Ready: 1});
      });
    });
    
    describe("then, a complete listener is added", function(){
      var listener;
      
      beforeEach(function(){
        listener = jasmine.createSpy("listener");
        provider.AddCompleteListener(listener);
      });
      
      it("the listener is called once with one ready component", function(){
        expect(listener).toHaveBeenCalledTimes(1);
        expect(listener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0, Loading: 0, Ready: 1 });
      });
    });
  });
  
  describe("resetting the service", function(){
    var dirtyListener, cleanListener, loadingListener, readyListener, completeListener;
    var listeners;
    
    beforeAll(function(){     
      listeners = {
        dirtyListener: function(){},
        cleanListener: function(){},
        loadingListener: function(){},
        readyListener: function(){},
        completeListener: function(){},
      }
      spyOn(listeners, "dirtyListener");
      spyOn(listeners, "cleanListener");
      spyOn(listeners, "loadingListener");
      spyOn(listeners, "readyListener");
      spyOn(listeners, "completeListener");
      
      provider.AddDirtyListener(listeners.dirtyListener);
      provider.AddCleanListener(listeners.cleanListener);
      provider.AddLoadingListener(listeners.loadingListener);
      provider.AddReadyListener(listeners.readyListener);
      provider.AddCompleteListener(listeners.completeListener);
      
      provider.Dirty("component1");
      provider.Clean("component2");
      provider.Loading("component3");
      provider.Ready("component4");
      
      listeners.dirtyListener.calls.reset();
      listeners.cleanListener.calls.reset();
      listeners.loadingListener.calls.reset();
      listeners.readyListener.calls.reset();
      listeners.completeListener.calls.reset();

      provider.Reset();
    });
    
    it("all listeners are called once with no component statuses", function(){
      expect(listeners.dirtyListener).toHaveBeenCalledTimes(1);
      expect(listeners.dirtyListener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0});
      expect(listeners.cleanListener).toHaveBeenCalledTimes(1);
      expect(listeners.cleanListener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0});
      expect(listeners.loadingListener).toHaveBeenCalledTimes(1);
      expect(listeners.loadingListener).toHaveBeenCalledWith({ Loading: 0, Ready: 0});
      expect(listeners.readyListener).toHaveBeenCalledTimes(1);
      expect(listeners.readyListener).toHaveBeenCalledWith({ Loading: 0, Ready: 0});
      expect(listeners.completeListener).toHaveBeenCalledTimes(1);
      expect(listeners.completeListener).toHaveBeenCalledWith({ Dirty: 0, Clean: 0, Loading: 0, Ready: 0});
    });
    
    it("reporter list has no entries", function(){
      expect(provider.GetReadyList().length).toEqual(0);
      expect(provider.GetLoadingList().length).toEqual(0);
      expect(provider.GetDirtyList().length).toEqual(0);
      expect(provider.GetCleanList().length).toEqual(0);
    });
  });
  
  describe("reporting an opposite status replaces it", function(){
    var foo;
    
    beforeEach(function(){
      foo = {
        completeListener: function(){},
      }
      spyOn(foo, "completeListener");
      provider.AddCompleteListener(foo.completeListener);
    });
    
    it("loading replaces ready and vice versa", function(){
      provider.Ready("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 0, Ready: 1, Dirty: 0, Clean: 0});
      provider.Loading("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 1, Ready: 0, Dirty: 0, Clean: 0});
      provider.Ready("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 0, Ready: 1, Dirty: 0, Clean: 0});
    });
    
    it("dirty replaces clean and vice versa", function(){
      provider.Dirty("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 1, Clean: 0});
      provider.Clean("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 0, Clean: 1});
      provider.Dirty("component");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 0, Ready: 0, Dirty: 1, Clean: 0});
    });
    
    it("loading & ready do not affect clean & dirty", function(){
      provider.Loading("component1");
      provider.Dirty("component2");
      expect(foo.completeListener).toHaveBeenCalledWith({ Loading: 1, Ready: 0, Dirty: 1, Clean: 0});
      expect(provider.GetLoadingList().length).toEqual(1);
      expect(provider.GetDirtyList().length).toEqual(1);
    });
  });
  
});