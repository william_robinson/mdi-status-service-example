describe("Delay Service", function(){
    var DelayService, $timeout;
    var randomMock = function(){
        var svc = function(value){
            // Ignore max value and just return 5.
            return 5; 
        }
        return svc;
    }
    beforeEach(function(){
        angular.module("mocks", []).factory("RandomInteger", randomMock);
        angular.module("test", ["plunker", "mocks"]);    
    });   
    beforeEach(module("test"));        
    beforeEach(inject(function(_Delay_, _$timeout_){
        DelayService = _Delay_;
        $timeout = _$timeout_;
    }));
    
    it("resolves a promise when a random time ends", function(){
        // Mocked to 5 seconds.
        var called = false;        
        DelayService(1).then(function(){
            called = true;    
        });
        
        $timeout.flush(2500); 
        expect(called).toBe(false);
        $timeout.flush(2500);
        expect(called).toBe(true);        
    });           
});