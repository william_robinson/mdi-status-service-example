describe("Random Integer Service", function(){
    var RandomInteger, mock;
    
    beforeEach(module("plunker"));
    beforeEach(inject(function(_RandomInteger_){
        RandomInteger = _RandomInteger_;
        mock = {
            RandomInteger: RandomInteger    
        }; 
        spyOn(mock, "RandomInteger").and.callThrough();
    }));
    
    it("throws an error if passed a non-number", function(){
        var test = function(val){
            return function(){
                mock.RandomInteger(val);
            }
        }
        expect(test("a")).toThrow();
        expect(test(NaN)).toThrow();            
    });
    
    it("returns an integer if passed a number", function(){
        var test = function(val){
            return Number.isInteger(mock.RandomInteger(val));
        }        
        expect(test(1)).toBe(true);
        expect(test(0.2)).toBe(true);
        expect(test("222")).toBe(true);
        expect(test(-5)).toBe(true);
    });
});