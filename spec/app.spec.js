describe("App Controller", function(){
    var $rootscope, $controller, $scope, appController, DelayedLoadReset, mdiStatus;
    
    beforeEach(module("plunker"));
    beforeEach(inject(function(_$rootScope_, _$controller_, _DelayedLoadReset_, _mdiStatus_){
        $rootscope = _$rootScope_;
        $controller = _$controller_;    
        DelayedLoadReset = _DelayedLoadReset_;
        mdiStatus = _mdiStatus_;
    }));
    beforeEach(function(){
        $scope = {};
        appController = $controller("MainCtrl", {$scope: $scope});        
    });
    
    it("is initialized when created", function(){
        expect(appController.Initialized).toEqual(true);
        expect(appController.ReadyToFire).toEqual(false);
        expect(appController.IgnitionReady).toEqual(false);
        expect(appController.TargetConfirmed).toEqual(false);
        expect(appController.Coordinates).toEqual("");
        expect(appController.Classification).toEqual(0);
        expect(appController.IgnitionTaskReady).toEqual([]);
        expect(appController.ShowFire).toEqual(false);
        expect(appController.ShowMonster).toEqual(false);
        expect(appController.FireImage).toEqual("");
    });
    
    describe("when reset", function(){
        var resetDelayListener, resetStatusListener;
        beforeEach(function(){            
            resetDelayListener = jasmine.createSpy("resetDelayListener");
            DelayedLoadReset.AddListener(resetDelayListener);

            resetStatusListener = jasmine.createSpy("resetStatusListener");
            mdiStatus.AddCompleteListener(resetStatusListener);

            appController.Reset();
        });
        
        it("resets all delayed loads", function(){
            expect(resetDelayListener).toHaveBeenCalled();
        });
        
        it("resets all component statuses", function(){
            expect(resetStatusListener).toHaveBeenCalled();
        });
        
        it("is initialized", function(){
            expect(appController.Initialized).toEqual(true);
            expect(appController.ReadyToFire).toEqual(false);
            expect(appController.IgnitionReady).toEqual(false);
            expect(appController.TargetConfirmed).toEqual(false);
            expect(appController.Coordinates).toEqual("");
            expect(appController.Classification).toEqual(0);
            expect(appController.IgnitionTaskReady).toEqual([]);
            expect(appController.ShowFire).toEqual(false);
            expect(appController.ShowMonster).toEqual(false);
            expect(appController.FireImage).toEqual("");
        });              
    });
    
    describe("responding to UI events", function(){
        var appStatus, statusSpy, mock;
        beforeEach(function(){
            mock = {
                statusListener: function(status){
                    appStatus = status;
                }
            };
            spyOn(mock, "statusListener").and.callThrough();
            mdiStatus.AddCompleteListener(mock.statusListener);
            mock.statusListener.calls.reset();            
        });
        
        it("sets 'coordinates' dirty when coordinates change", function(){
           appController.CoordinatesChanged();
           expect(mock.statusListener).toHaveBeenCalled();
           expect(appStatus.Dirty).toEqual(1);
           expect(mdiStatus.GetDirtyList()).toContain("coordinates"); 
        });
        
        it("sets 'classification' dirty when a classification is chosen", function(){
            appController.ClassificationChanged();
            expect(mock.statusListener).toHaveBeenCalled();
            expect(appStatus.Dirty).toEqual(1);
            expect(mdiStatus.GetDirtyList()).toContain("classification");
        });
        
        it("sets 'coordinates' and 'classification' clean when target is confirmed", function(){
            appController.CoordinatesChanged();
            appController.ClassificationChanged();
            mock.statusListener.calls.reset();
            appController.ConfirmTarget();
            expect(mock.statusListener).toHaveBeenCalledTimes(2);
            expect(appStatus.Clean).toEqual(2);
            expect(mdiStatus.GetCleanList()).toContain("classification");
            expect(mdiStatus.GetCleanList()).toContain("coordinates");
        });
    });
    
    describe("before target is confirmed and systems are initialized", function(){
        it("should not be ready to fire", function(){
            expect(appController.ReadyToFire).toBe(false);
        })
        it("throws an error when weapon is fired", function(){
            expect(appController.Fire).toThrow();
        });
    });

    describe("after target is confirmed and systems are initialized", function(){
        beforeEach(function(){
            appController.ConfirmTarget();
            mdiStatus.Ready("1");
            mdiStatus.Ready("2");
            mdiStatus.Ready("3");                
        });  
        it("should be ready to fire", function(){
            expect(appController.ReadyToFire).toBe(true);
        })
        describe("when weapon is fired", function(){
            var listen;
            beforeEach(function(){
                jasmine.clock().install();
                appController.Fire();
                listen = jasmine.createSpy("listen");                
            });            
            afterEach(function(){
                jasmine.clock().uninstall();
            });
            
            it("shows fire animation", function(){
                expect(appController.ShowFire).toBe(true);
            });
            it("sets fire image", function(){
                expect(appController.FireImage).toEqual({"background-image": "url(http://fansided.com/files/2014/12/Ku7mUmo.gif)"});
            });
            it("resets after 4250 ms", inject(function($timeout){
                $timeout.flush(4250);
                expect(appController.ShowFire).toBe(false);
            }));
            it("blinks 'You Monster' three times", inject(function($interval){
                expect(appController.ShowMonster).toBe(false);
                $interval.flush(500);
                expect(appController.ShowMonster).toBe(true);
                $interval.flush(500);
                expect(appController.ShowMonster).toBe(false);
                $interval.flush(500);
                expect(appController.ShowMonster).toBe(true);
                $interval.flush(500);
                expect(appController.ShowMonster).toBe(false);
                $interval.flush(500);
                expect(appController.ShowMonster).toBe(true);
            }));
        });            
    });
});