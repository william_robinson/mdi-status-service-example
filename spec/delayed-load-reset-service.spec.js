describe("Delayed Load Reset Service", function(){
    var DelayedLoadReset;
    beforeEach(module("plunker"));
    beforeEach(inject(function(_DelayedLoadReset_){
        DelayedLoadReset = _DelayedLoadReset_;
    }));
    
    it("calls registered listeners when invoked", function(){
        var listener = jasmine.createSpy("listener"); 
        DelayedLoadReset.AddListener(listener);
        DelayedLoadReset();
        expect(listener).toHaveBeenCalled();        
    });
});
