describe("Delayed Load Directive", function () {
    var $rootScope, elm, DelayedLoadReset;
    var readyGlyph = String.fromCharCode(9746);
    var loadingGlyph = String.fromCharCode(9744);
    var loadedGlyph = String.fromCharCode(9745);

    // Setup mocks 
    var mockDelay = function($q){
        return function(int){
            var task = $q.defer();
            setTimeout(function(){
                task.resolve();
            }, 1000);
            return task.promise;
        };
    };       
    beforeEach(function(){
        angular.module("mocks",[]).factory("Delay", mockDelay);
        angular.module("test", ["plunker", "mocks"])
    })
    
    // Initialize modules    
    beforeEach(module("test"));       
    beforeEach(module("templates/delayed-load.html"));
    
    // Prepare directive 
    beforeEach(inject(function ($compile, _$rootScope_, _DelayedLoadReset_) {                         
        elm = angular.element("<delayed-load action='DoAction'></delayed-load>");
        DelayedLoadReset = _DelayedLoadReset_;

        $rootScope = _$rootScope_;
        $compile(elm)($rootScope);
        $rootScope.$digest();
    }));    
    
    it("starts showing unloaded icon", function () {
        expect(elm).toContainText(readyGlyph);
        expect(elm).not.toContainText(loadingGlyph);
        expect(elm).not.toContainText(loadedGlyph);
    });
    
    it("starts with button enabled", function(){
        expect(elm.find("input:button")).not.toBeDisabled();
    });                  

    
    describe("when button is clicked", function(){
        var originalTimeout;
        
        beforeEach(function(){
            jasmine.clock().install();
            elm.isolateScope().Load();  
        });
        
        afterEach(function(){
            jasmine.clock().uninstall();    
        });
             
        it("shows loading icon", function(){
            jasmine.clock().tick(100);
            $rootScope.$apply(); // Allow promise to resolve
            expect(elm).not.toContainText(readyGlyph);
            expect(elm).toContainText(loadingGlyph);
            expect(elm).not.toContainText(loadedGlyph);
        });
        it("shows finished icon after enough time has passed", function(){
            jasmine.clock().tick(1001);
            $rootScope.$apply();
            expect(elm).not.toContainText(readyGlyph);
            expect(elm).not.toContainText(loadingGlyph);
            expect(elm).toContainText(loadedGlyph);
        }); 
        it("disables button", function(){
            jasmine.clock().tick(1);
            $rootScope.$apply();
            expect(elm.find("input[type=button]")).toBeDisabled();
        });                  
        it("returns to original state when reset", function(){
            jasmine.clock().tick(1001);
            $rootScope.$apply();    
            DelayedLoadReset();
            $rootScope.$apply(); // Fire all watches
            expect(elm).toContainText(readyGlyph);
            expect(elm).not.toContainText(loadingGlyph);
            expect(elm).not.toContainText(loadedGlyph);
        });      
        
    });    
});